from krita import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import os


class ClickableImageLabel(QLabel):

    clickedWithImagePath = pyqtSignal(str)

    def __init__(self, image_path):
        super().__init__()
        self.image_path = image_path

        image = QImage(image_path)
        side_length = min(image.width(), image.height())
        self.cropped_image = image.copy((image.width() - side_length) // 2,
                                        (image.height() - side_length) // 2, side_length, side_length)
        pixmap = QPixmap.fromImage(self.cropped_image)
        self.setPixmap(pixmap.scaled(50, 50, Qt.KeepAspectRatio))

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            drag = QDrag(self)
            mime_data = QMimeData()
            mime_data.setText(self.image_path)
            drag.setMimeData(mime_data)

            # Set the appearance of the dragged pixmap
            pixmap = self.pixmap()
            drag.setPixmap(pixmap)

            # Set the hotspot for the drag (where the cursor position aligns with the pixmap)
            drag.setHotSpot(event.pos() - self.rect().topLeft())

            # Start the drag operation
            drag.exec_(Qt.MoveAction)

    def on_clicked(self):
        self.clickedWithImagePath.emit(self.image_path)


class DropPushButton(QLabel):

    droppedWithImagePath = pyqtSignal(str)
    clicked = pyqtSignal()  # Custom clicked signal

    def __init__(self, text="", parent=None):
        super().__init__(text, parent)
        self.setAcceptDrops(True)
        self.setStyleSheet("border: 1px solid white")
        self.image_path = None

    def dragEnterEvent(self, event):
        event.acceptProposedAction()

    def dropEvent(self, event):
        self.image_path = event.mimeData().text()
        image = QImage(self.image_path)
        side_length = min(image.width(), image.height())
        self.cropped_image = image.copy((image.width() - side_length) // 2,
                                        (image.height() - side_length) // 2, side_length, side_length)
        pixmap = QPixmap.fromImage(self.cropped_image)
        self.setPixmap(pixmap.scaled(50, 50, Qt.KeepAspectRatio))
        self.droppedWithImagePath.emit(self.image_path)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            if self.image_path is not None:
                self.clear()
                self.image_path = None
            self.clicked.emit()


class ButtonGridDialog(QDialog):

    def __init__(self):
        super().__init__()

        folder_path = self.browse_file()

        self.image_paths = []
        self.selectedPositions = []

        self.gridSizeLayout = QHBoxLayout()

        # Create layout for buttons
        self.layoutForButtons = QGridLayout()

        self.selected_image_paths = []
        self.selectedButtons = {}

        self.okButton = QPushButton("OK")
        self.okButton.clicked.connect(self.acceptSelectedButtons)

        reference_image_grid = QScrollArea()
        reference_image_grid.setWidgetResizable(True)

        content_widget = QWidget()
        reference_image_grid.setWidget(content_widget)
        layout = QGridLayout(content_widget)

        image_files = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(
            folder_path, f)) and f.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.bmp'))]

        row = 0
        col = 0

        for image_file in image_files:
            image_path = os.path.join(folder_path, image_file)
            clickable_label = ClickableImageLabel(image_path)
            clickable_label.clickedWithImagePath.connect(self.image_clicked)

            layout.addWidget(clickable_label, row, col)
            col += 1

            if col >= 10:  # Set the number of columns as desired
                col = 0
                row += 1

        # Create buttons and add to the layout based on input rows and columns
        for i in range(3):
            for j in range(10):
                button = DropPushButton()
                # Set fixed size for buttons (width, height)
                button.setFixedSize(50, 50)
                self.selectedButtons[(i, j)] = button
                self.layoutForButtons.addWidget(button, i, j)

        # Create main layout and add sub-layouts
        self.mainLayout = QVBoxLayout()
        self.mainLayout.addWidget(reference_image_grid)
        self.mainLayout.addLayout(self.gridSizeLayout)
        self.mainLayout.addLayout(self.layoutForButtons)
        self.mainLayout.addWidget(self.okButton)

        self.setLayout(self.mainLayout)
        self.setWindowTitle("Button Grid Generator")

    def acceptSelectedButtons(self):
        self.selectedPositions = [(position, button.image_path) for position,
                                  button in self.selectedButtons.items()]

        print(self.selectedPositions)

        self.accept()

    def browse_file(self):
        return QFileDialog.getExistingDirectory(
            None, "Select a folder", "")

    def image_clicked(self, image_path):
        if image_path in self.image_paths:
            self.image_paths.remove(image_path)
        else:
            self.image_paths.append(image_path)
        # Placeholder action, change as needed
        print("Selected Images:", self.image_paths)


# Define your plugin class
class PracticeGridPlugin(Extension):
    def __init__(self, parent):
        super().__init__(parent)

    def setup(self):
        pass

    def createFileLayers(self):
        DIMENSION = (320, 500)
        xboxes = [0, 320, 640, 960, 1280, 1600, 1920, 2240, 2560, 2880]
        yboxes = [0, 500, 1000]

        newDialog = ButtonGridDialog()
        newDialog.exec_()

        # create new document createDocument(width, height, name, colorSpace, bitDepth, colorProfile, DPI)
        new_doc = Krita.instance().createDocument(
            3200, 1500, "Exercises", "RGBA", "U8", "", 300.0)
        Krita.instance().activeWindow().addView(new_doc)  # shows it in the application
        references = new_doc.createGroupLayer('references')
        root = new_doc.rootNode()
        root.addChildNode(references, None)

        for ((row, col), filepath) in newDialog.selectedPositions:
            if filepath != None:
                print(row, col, filepath)
                reference_group = new_doc.createGroupLayer(
                    f'{row}-{col}')
                references.addChildNode(reference_group, None)

                # Create a new layer for each image file
                createFileLayer = new_doc.createFileLayer(
                    f'reference', filepath, "none")
                reference_group.addChildNode(createFileLayer, None)
                width = createFileLayer.bounds().width()
                height = createFileLayer.bounds().height()
                scale_factor = 1
                if width <= height:
                    scale_factor = DIMENSION[1] / height
                else:
                    scale_factor = DIMENSION[0] / width

                transform_mask = new_doc.createTransformMask(
                    "transform")
                createFileLayer.addChildNode(transform_mask, None)
                print(f'{row}-{col}')
                xml = f"""
                        <!DOCTYPE transform_params>
                        <transform_params>
                        <main id="tooltransformparams"/>
                        <data mode="0">
                        <free_transform>
                        <transformedCenter type="pointf" x="{xboxes[col]}" y="{yboxes[row]}"/>
                        <originalCenter type="pointf" x="0" y="0"/>
                        <rotationCenterOffset type="pointf" x="0" y="0"/>
                        <transformAroundRotationCenter type="value" value="0"/>
                        <aX type="value" value="0"/>
                        <aY type="value" value="0"/>
                        <aZ type="value" value="0"/>
                        <cameraPos type="vector3d" x="0" z="1024" y="0"/>
                        <scaleX type="value" value="{scale_factor}"/>
                        <scaleY type="value" value="{scale_factor}"/>
                        <shearX type="value" value="0"/>
                        <shearY type="value" value="0"/>
                        <keepAspectRatio type="value" value="0"/>
                        <flattenedPerspectiveTransform type="transform" m13="0" m33="1" m21="0" m11="1" m31="0" m32="0" m12="0" m22="1" m23="0"/>
                        <filterId type="value" value="Bicubic"/>
                        </free_transform>
                        </data>
                        </transform_params>
                        """
                transform_mask.fromXML(xml)

                paint_layer = new_doc.createNode("paintover", "paintLayer")
                reference_group.addChildNode(paint_layer, None)

                sel = Selection()
                sel.select(xboxes[col], yboxes[row],
                           DIMENSION[0], DIMENSION[1], 255)

                tm = new_doc.createTransparencyMask('transparency')
                tm.setSelection(sel)
                tm.setLocked(True)
                reference_group.addChildNode(tm, None)

            else:

                reference_group = new_doc.createGroupLayer(
                    f'{row}-{col}')
                references.addChildNode(reference_group, None)

                sel = Selection()
                sel.select(xboxes[col], yboxes[row],
                           DIMENSION[0], DIMENSION[1], 255)

                setColor = InfoObject()
                setColor.setProperty('color', '#FFFFFF')

                fillLayer = new_doc.createFillLayer(
                    'shading', 'color', setColor, sel)
                reference_group.addChildNode(fillLayer, None)

                paint_layer = new_doc.createNode("paintover", "paintLayer")
                paint_layer.setInheritAlpha(True)
                reference_group.addChildNode(paint_layer, None)

        # TODO Create borders dynamically
        borders = new_doc.createFileLayer(
            f'borders', f'borders.png', "none")
        root.addChildNode(borders, None)

        new_doc.refreshProjection()

    def createActions(self, window):
        action = window.createAction(
            "CREATE_GRID_PLUGIN", "Create image grid", "tools/scripts")
        print(action)
        action.triggered.connect(self.createFileLayers)
