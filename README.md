# Krita Practice Grid Plugin

## Description

Generate a grid filled with reference images for art practice

## Installation

Move the contents of this repo into the specific krita plugin directory for your platform

## Usage

Drag and drop your reference images into place and start practicing them in a grid

![Practice Grid Setup](img/setup.png)

Paint only in the specific region you want without worrying of going out drawing into other adjacent cells

![Practice Grid Usage](img/grid.png)

## Credits

Special thanks to [AhabGreybeard](https://krita-artists.org/u/AhabGreybeard/summary) and [KnowZero](https://krita-artists.org/u/KnowZero/summary) for answering my questions about the (sometimes rather cryptic) krita python sdk!

## License

GPL-3.0

---
